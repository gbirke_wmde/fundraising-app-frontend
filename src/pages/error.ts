import Vue from 'vue';
import VueI18n from 'vue-i18n';
import VueCompositionApi from '@vue/composition-api';
import PageDataInitializer from '@/page_data_initializer';
import { DEFAULT_LOCALE } from '@/locales';
import App from '@/components/App.vue';

import Component from '@/components/pages/Error.vue';
import Sidebar from '@/components/layout/Sidebar.vue';
import createCookieConsent from '@/cookie_consent';

const PAGE_IDENTIFIER = 'error-page';

Vue.config.productionTip = false;
Vue.use( VueI18n );
Vue.use( VueCompositionApi );

interface ErrorModel {
	message: string,
	trace: string,
}

const pageData = new PageDataInitializer<ErrorModel>( '#appdata' );

const i18n = new VueI18n( {
	locale: DEFAULT_LOCALE,
	messages: {
		[ DEFAULT_LOCALE ]: pageData.messages,
	},
} );

new Vue( {
	i18n,
	provide: {
		cookieConsent: createCookieConsent( pageData.cookieConsent ),
	},
	render: h => h( App, {
		props: {
			assetsPath: pageData.assetsPath,
			pageIdentifier: PAGE_IDENTIFIER,
		},
	},
	[
		h( Component, {
			props: {
				errorMessage: pageData.applicationVars.message,
				errorTrace: pageData.applicationVars.trace,
			},
		} ),
		h( Sidebar, {
			slot: 'sidebar',
		} ),
	] ),
} ).$mount( '#app' );
