export interface Donation {
	accessToken: String,
	amount: Number,
	bankTransferCode: String,
	cookieDuration: String,
	creationDate: String,
	id: Number,
	interval: Number,
	optsIntoDonationReceipt: Boolean,
	optsIntoNewsletter: Boolean,
	paymentType: String,
	status: String
	updateToken: String,
}
