import { mount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import Buefy from 'buefy';
import CompositionAPI from '@vue/composition-api';
import axios from 'axios';

import CookieNotice from '@/components/cookie_notice/CookieNotice.vue';
import createCookieConsent, { CONSENT_STATE, CookieConsentInterface } from '@/cookie_consent';

jest.mock( 'axios', () => ( {
	post: jest.fn( () => Promise.resolve( { 'status': 'OK' } ) ),
} ) );

const localVue = createLocalVue();
localVue.use( Vuex );
localVue.use( Buefy );
localVue.use( CompositionAPI );

const getWrapperWithCookieConsent = ( cookieConsent: CookieConsentInterface ) => {
	return mount( CookieNotice, {
		localVue,
		mocks: {
			$t: ( key: string ) => key,
		},
		provide: {
			cookieConsent,
		},
	} );
};

describe( 'CookieNotice', () => {
	beforeEach( () => {
		jest.resetModules();
		jest.clearAllMocks();
	} );

	it( 'shows when no consent choice has been submitted', async () => {
		const wrapper = getWrapperWithCookieConsent( createCookieConsent( 'unset' ) );

		expect( wrapper.find( '.cookie-notice' ).exists() ).toBe( true );
	} );

	it( 'does not show when consent choice has been submitted', async () => {
		let yesWrapper = getWrapperWithCookieConsent( createCookieConsent( 'yes' ) );
		let noWrapper = getWrapperWithCookieConsent( createCookieConsent( 'no' ) );

		expect( yesWrapper.find( '.cookie-notice' ).exists() ).toBe( false );
		expect( noWrapper.find( '.cookie-notice' ).exists() ).toBe( false );
	} );

	it( 'changes view when check and back buttons are clicked', async () => {
		const wrapper = getWrapperWithCookieConsent( createCookieConsent( 'unset' ) );
		wrapper.find( '.check > button' ).trigger( 'click' );
		await wrapper.vm.$nextTick();

		expect( ( wrapper.vm as any ).showOptions ).toBeTruthy();

		wrapper.find( '.cookie-notice-back-button' ).trigger( 'click' );
		await wrapper.vm.$nextTick();

		expect( ( wrapper.vm as any ).showOptions ).toBeFalsy();
	} );

	it( 'submits consent when accept button is clicked', async () => {
		const cookieConsent = createCookieConsent( 'unset' );
		const wrapper = getWrapperWithCookieConsent( cookieConsent );

		wrapper.find( '.accept > button' ).trigger( 'click' );
		await wrapper.vm.$nextTick();

		expect( cookieConsent.consentState.value ).toEqual( CONSENT_STATE.TRUE );
	} );

	it( 'submits positive consent when save button is clicked and consent given', async () => {
		const cookieConsent = createCookieConsent( 'unset' );
		const wrapper = getWrapperWithCookieConsent( cookieConsent );

		wrapper.find( '.check > button' ).trigger( 'click' );
		await wrapper.vm.$nextTick();
		await wrapper.find( 'input[name=optional]' ).trigger( 'change' );
		await wrapper.find( '.save > button' ).trigger( 'click' );

		expect( cookieConsent.consentState.value ).toEqual( CONSENT_STATE.TRUE );
	} );

	it( 'submits negative consent no when save button is clicked and consent not given', async () => {
		const cookieConsent = createCookieConsent( 'unset' );
		const wrapper = getWrapperWithCookieConsent( cookieConsent );

		wrapper.find( '.check > button' ).trigger( 'click' );
		await wrapper.vm.$nextTick();
		wrapper.find( '.save > button' ).trigger( 'click' );
		await wrapper.vm.$nextTick();

		expect( cookieConsent.consentState.value ).toEqual( CONSENT_STATE.FALSE );
	} );
} );
